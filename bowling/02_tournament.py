# -*- coding: utf-8 -*-
# Python 3.7
import argparse
import operator
from tournament_score import Tournament


def add_argument():
    parser = argparse.ArgumentParser(description='Сумма очков.')
    parser.add_argument('--input', type=str, help='Введите путь к файлу для расчета.')
    parser.add_argument('--output', type=str, help='Введите имя файла для результатов.')
    name_space = parser.parse_args()
    return name_space


def line():  # ;D
    return f'{"+":-<11}{"+":-<19}{"+":-<15}+'


def on_print():
    print(line())
    print(f'| {"Игрок":<9}|{"сыграно матчей":^18}|{"всего побед":^14}|')
    print(line())
    for name, count_winner_and_score_ in sorted_dict:
        score = count_winner_and_score_[1]
        count_winner = count_winner_and_score_[0]
        print(f'| {name:<9}|{score:^18}|{count_winner:^14}|')
    print(line())


if __name__ == '__main__':
    my_name_space = add_argument()
    result = Tournament(input_file=my_name_space.input, output_file=my_name_space.output)
    result.check_file_exist()  # Если файл существует, мы его удаляем.
    result.read_file()
    return_dict = result.tournament_result.items()  # Возврящает сформированный словарь из имен, кол-во игр, побед.
    sorted_dict = sorted(return_dict, key=operator.itemgetter(1), reverse=True)
    on_print()
