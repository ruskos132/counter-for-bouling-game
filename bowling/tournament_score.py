# -*- coding: utf-8 -*-
# Python 3.7

import os
from my_score import GetScoreNew
from collections import defaultdict
import logging


logging.basicConfig(level=logging.INFO, format='%(message)s')


def get_result(one_people_result):
    game_result = one_people_result
    get_score = GetScoreNew(game_result=game_result)
    get_score.frame_generator()
    return_result = get_score.run()
    return return_result


class Tournament:
    """
    Python 3.7
    """
    winner_score = 0
    winner_name = ''
    tournament_result = defaultdict(list)
    count_games = 0

    def __init__(self, input_file, output_file='result.txt'):
        """
        :param input_file: Имя и путь к файлу.
        :param output_file: Имя и путь к файлу для записи результатов. По умолчанию имя файла result.txt.
        """
        self.tournament = input_file
        self.file_output_name = output_file

    def check_file_exist(self):
        """
        Если файл присутствует то он удаляется.
        :return: None
        """
        if os.path.exists(self.file_output_name):
            os.remove(self.file_output_name)

    def read_file(self):
        """Чтение файл, поиск победителя, формирование результатов всего турнира."""

        with open(self.tournament, mode='r', encoding='utf8') as row_file:
            for row_line in row_file:
                if row_line[:6] != 'winner' and row_line[:3] != '###' and row_line != '\n':
                    line = row_line.strip().split('\t')
                    score = get_result(line[1])
                    self.logic(line, score)

                elif row_line[:3] == '###':
                    self.winner_score = 0
                    self.winner_name = ''
                    self.write_file_output(information=f'{row_line.strip()}\n')
                    logging.debug(row_line.strip())

                elif row_line[:6] != 'winner':
                    if self.winner_name:
                        self.write_file_output(information=f'winner is {self.winner_name}\n')
                        self.tournament_result[self.winner_name][0] += 1
                        logging.debug(f'winner is {self.winner_name}\n')
                    else:
                        self.write_file_output(information='winner is None\n')
                        logging.debug('winner is None\n')

    def logic(self, line, score):
        """
        Основаня часть read_file.
        :param line: одна строка файла
        :param score: количесто очков
        :return: None
        """
        if isinstance(score, tuple):
            if self.winner_score == 0:
                self.winner_score = score[1]
                self.winner_name = line[0]
            elif self.winner_score <= score[1]:
                self.winner_score = score[1]
                self.winner_name = line[0]
            self.write_file_output(information=f'{line[0]:<15}{score[0]:<30}{score[1]}\n')

            if self.winner_name in self.tournament_result:
                self.tournament_result[self.winner_name][1] += 1
            else:
                self.tournament_result.update({self.winner_name: [1, 0]})
            logging.debug(f'{line[0]:<15}{score[0]:<30}{score[1]}')

        else:
            self.write_file_output(information=f'{line[0]:<15}{"Error":<30}{"0"}\n')
            logging.debug(f'{line[0]:<15}{"Error":<30}{"0"}')  # Сообщение с ошибкой.

    def write_file_output(self, information):
        """
        Открытие и запись файла в файл.
        :param information: Информация которую нужно записать в файл.
        :return: None
        """
        with open(self.file_output_name, mode='a', encoding='utf8') as file:
            file.write(information)


file_name = 'tournament.txt'
row_file_path = os.path.realpath(file_name)
file_path = os.path.normpath(row_file_path)


if __name__ == '__main__':
    result = Tournament(input_file=file_path)
    result.check_file_exist()
    result.read_file()
