# -*- coding: utf-8 -*-
# Python 3.7


class LenError(Exception):
    def __init__(self):
        pass


class SingleFrameError(Exception):
    def __init__(self):
        pass


class SymbolPositionError(Exception):
    def __init__(self):
        pass


class RusSymbolError(Exception):
    def __init__(self):
        pass


class GetScoreOld:
    """
    Python 3.7
    Программа для подсчета очков в боулинг (rus)
    Program for get score in a bowling game (eng)
    """

    FRAME_LIMIT = 10

    def __init__(self, game_result):
        """
        :param game_result:
        """
        self.game_result = game_result
        self.result = 0
        self.err_frame = None
        self.ready_list = []
        self.ready_list_new = []

    def frame_generator(self):
        """
        Генерирует фреймы в зависимости от Х. Если не Х то в фрейме два значения, иначе одно.
        :return: self.ready_list
        """
        start, stop = 0, 2
        for _ in range(self.FRAME_LIMIT + 1):
            check_frame = self.game_result[start:stop]
            if 'X' in check_frame or 'Х' in check_frame:
                frame = self.game_result[start:stop - 1]
                self.ready_list.append(frame)
                start += 1
                stop += 1
            else:
                self.ready_list.append(check_frame)
                start += 2
                stop += 2

        while '' in self.ready_list:
            self.ready_list.remove('')

        # print(self.ready_list)  # для проверки

    def run(self):
        """
        :return: возвращает результат работы на консоль.
        """
        try:
            self.err_check()
        except ValueError:
            return f'Неверное значение: {[self.err_frame]}'
        except LenError:
            return f'{LenError.__name__}: В веденных данных не 10 фреймов.'
        except SingleFrameError:
            return f'{SingleFrameError.__name__}: Все фреймы кроме "Х" должны быть парными. {[self.err_frame]}'
        except SymbolPositionError:
            return f'{SymbolPositionError.__name__}: Символ "/" должен быть в конце фрейма. {[self.err_frame]}'
        except RusSymbolError:
            return f'{RusSymbolError.__name__}: Русский символ {[self.err_frame]}'

        return self.game_result, self.result

    def err_check(self):
        """
        Проверка ошиок в фреймах.
        :return: None
        """
        for symbol in self.ready_list:
            self.err_frame = symbol
            if len(self.ready_list) != 10:
                raise LenError
            elif len(self.ready_list) > 10:
                raise LenError
            elif symbol == 'Х':
                raise RusSymbolError
            elif len(symbol) == 1 and symbol != 'X':
                raise SingleFrameError
            elif symbol[0] == '/':
                raise SymbolPositionError
            elif len(symbol) == 2 and '-' not in symbol and '/' not in symbol and int(symbol[0]) + int(symbol[1]) >= 10:
                raise ValueError

        self.calculation()

    def calculation(self):
        """Старый калькулятор очков"""
        for symbol in self.ready_list:
            if symbol == 'X':
                self.result += 20
            elif symbol[1] == '/':
                self.result += 15
            elif symbol[:2] == '--':
                self.result += 0
            elif symbol[0] == '-':
                self.result += int(symbol[1])
            elif symbol[1] == '-':
                self.result += int(symbol[0])
            else:
                self.result += (int(symbol[0]) + int(symbol[1]))


class GetScoreNew(GetScoreOld):
    """Новый метод подсчета очков."""

    def refactor_for_x(self, index):
        """
        Если в self.ready_list есть символ 'X':
        :param index: Позиция элемента 'X' в self.ready_list
        :return: None
        """
        self.ready_list_new.append(self.ready_list[index])

        if index < self.FRAME_LIMIT - 2:
            self.ready_list_new.append(self.ready_list[(index + 1) - len(self.ready_list)])
            self.ready_list_new.append(self.ready_list[(index + 2) - len(self.ready_list)])

        elif index < self.FRAME_LIMIT - 1:
            self.ready_list_new.append(self.ready_list[(index + 1) - len(self.ready_list)])

    def refactor_for_slash(self, index):
        """
        Если в self.ready_list есть фрейм с символом '/':
        :param index: Позиция фрейма с элементом '/' в self.ready_list
        :return: None
        """
        self.ready_list_new.append(self.ready_list[index])

        if index < self.FRAME_LIMIT - 1:
            num = self.ready_list[(index + 1) - len(self.ready_list)]
            if num != 'X':
                self.ready_list_new.append(f'{num[0]}-')
            else:
                self.ready_list_new.append(f'{num}')

    def refactor(self):
        """
        Изменяет self.ready_list на self.ready_list_new для дальнейшего подсчета очков
        :return: None
        """
        for index, symbol in enumerate(self.ready_list):
            if symbol == 'X':
                self.refactor_for_x(index=index)
            elif '/' in symbol:
                self.refactor_for_slash(index=index)
            else:
                self.ready_list_new.append(self.ready_list[index])

    def calculation(self):
        """Новый калькулятор очков"""

        self.refactor()

        for symbol in self.ready_list_new:
            if symbol == 'X':
                self.result += 10
            elif symbol[1] == '/':
                self.result += 10
            elif symbol[:2] == '--':
                self.result += 0
            elif symbol[0] == '-':
                self.result += int(symbol[1])
            elif symbol[1] == '-':
                self.result += int(symbol[0])
            else:
                self.result += (int(symbol[0]) + int(symbol[1]))

        # print(self.ready_list_new, self.ready_list_new.count('X'))  # для проверки


if __name__ == '__main__':

    get_score = GetScoreNew(game_result='X4/34-45-X8/X---1')
    get_score.frame_generator()
    return_result = get_score.run()

    if len(return_result) == 2:
        print(f'Вы ввели {return_result[0]}, это - {return_result[1]} очков')
    else:
        print(return_result)
