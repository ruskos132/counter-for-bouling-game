# -*- coding: utf-8 -*-
# Python 3.7
import argparse
from my_score import GetScoreNew


def add_argument():
    parser = argparse.ArgumentParser(description='Сумма очков.')
    parser.add_argument('--result', type=str, help='Введите результат игры для расчета суммы очков.')
    name_space = parser.parse_args()
    return name_space


if __name__ == '__main__':
    my_name_space = add_argument()
    get_score = GetScoreNew(game_result=my_name_space.result)
    get_score.frame_generator()
    return_result = get_score.run()

    if len(return_result) == 2:
        print(f'Вы ввели {return_result[0]}, это - {return_result[1]} очков')
    else:
        print(return_result)
