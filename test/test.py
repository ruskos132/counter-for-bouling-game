# -*- coding: utf-8 -*-
# Python 3.7

import unittest

from bowling import GetScoreOld, GetScoreNew


class MyResultTest(unittest.TestCase):

    def test_normal_sum_1(self):
        get_score = GetScoreOld(game_result='X4/34-45-X8/X---1')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[1], 107)

    def test_normal_sum_2(self):
        get_score = GetScoreOld(game_result='XXXXXXXXXX')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[1], 200)

    def test_normal_sum_3(self):
        get_score = GetScoreNew(game_result='XXXXXXXXXX')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[1], 270)

    def test_normal_sum_4(self):
        get_score = GetScoreNew(game_result='X---253X545/2/8/02')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[1], 102)

    def test_rus_symbol(self):
        get_score = GetScoreOld(game_result='Х4/34-45-X8/X---1')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result, "RusSymbolError: Русский символ ['Х']")

    def test_single_frame(self):
        get_score = GetScoreOld(game_result='4X/34-45-X8/X--')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[0:16], 'SingleFrameError')

    def test_incorrect_value(self):
        get_score = GetScoreOld(game_result='X/434-45-X8/X---1')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[0:19], 'SymbolPositionError')

    def test_incorrect_len_0(self):
        get_score = GetScoreOld(game_result='X4/34-45-X8/--')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[0:8], 'LenError')

    def test_incorrect_len_1(self):
        get_score = GetScoreOld(game_result='X4/34-45-X8/--902/14')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[0:8], 'LenError')

    def test_incorrect_sum_value(self):
        get_score = GetScoreOld(game_result='734/34-45-X8/X---1')
        get_score.frame_generator()
        return_result = get_score.run()
        self.assertEqual(return_result[0:17], 'Неверное значение')


if __name__ == '__main__':
    unittest.main()
